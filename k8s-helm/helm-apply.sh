#!/bin/bash
# Files are ordered in proper order with needed wait for the dependent custom resource definitions to get initialized.
# Usage: bash helm-apply.sh
cs=csvc
suffix=helm
kubectl apply -f namespace.yml
helmVersion=$(helm version --client | grep -E "v3\\.[0-9]{1,3}\\.[0-9]{1,3}" | wc -l)
if [ -d "${cs}-${suffix}" ]; then
  if [ $helmVersion -eq 1 ]; then
helm uninstall ${cs} 2>/dev/null
  else
helm delete --purge ${cs} 2>/dev/null
  fi
helm dep up ./${cs}-${suffix}
  if [ $helmVersion -eq 1 ]; then
helm install ${cs} ./${cs}-${suffix} --replace --namespace ocs
  else
helm install --name ${cs} ./${cs}-${suffix} --replace --namespace ocs
  fi
fi
  if [ $helmVersion -eq 1 ]; then
helm uninstall app 2>/dev/null
  else
helm delete --purge app 2>/dev/null
  fi
helm dep up ./app-${suffix}
  if [ $helmVersion -eq 1 ]; then
helm install app  ./app-${suffix} --replace --namespace ocs
  else
helm install --name app  ./app-${suffix} --replace --namespace ocs
  fi
  if [ $helmVersion -eq 1 ]; then
helm uninstall gateway 2>/dev/null
  else
helm delete --purge gateway 2>/dev/null
  fi
helm dep up ./gateway-${suffix}
  if [ $helmVersion -eq 1 ]; then
helm install gateway  ./gateway-${suffix} --replace --namespace ocs
  else
helm install --name gateway  ./gateway-${suffix} --replace --namespace ocs
  fi
  if [ $helmVersion -eq 1 ]; then
helm uninstall sso 2>/dev/null
  else
helm delete --purge sso 2>/dev/null
  fi
helm dep up ./sso-${suffix}
  if [ $helmVersion -eq 1 ]; then
helm install sso  ./sso-${suffix} --replace --namespace ocs
  else
helm install --name sso  ./sso-${suffix} --replace --namespace ocs
  fi


