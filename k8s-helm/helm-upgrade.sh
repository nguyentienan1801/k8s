#!/bin/bash
# Files are ordered in proper order with needed wait for the dependent custom resource definitions to get initialized.
# Usage: bash helm-apply.sh
cs=csvc
suffix=helm
if [ -d "${cs}-${suffix}" ]; then
helm dep up ./${cs}-${suffix}
helm upgrade --install ${cs} ./${cs}-${suffix} --namespace ocs
fi
helm dep up ./app-${suffix}
helm upgrade --install app ./app-${suffix} --namespace ocs
helm dep up ./gateway-${suffix}
helm upgrade --install gateway ./gateway-${suffix} --namespace ocs
helm dep up ./sso-${suffix}
helm upgrade --install sso ./sso-${suffix} --namespace ocs


